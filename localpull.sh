#!/usr/bin/env bash
if [ `id -u` -eq 0 ]; then
    echo "Error: Do not run this script as root or with sudo" >&2
    exit 0
fi

out=$HOME/nixos-config

# pulling configuration.nix and system modules from /etc/nixos
sudo rm -vrf $out/system/*
sudo cp -vr /etc/nixos/* $out/system

# pulling home-manager modules from ~/.config/nixpkgs
rm -vrf $out/user/*
cp -vr $HOME/.config/nixpkgs/* $out/user

# pulling non-nixos configs from other places
cp -vr $HOME/.config/nvim $out/assets               # neovim
cp -vr $HOME/.config/qtile $out/assets              # qtile
cp -vr $HOME/.config/polybar $out/assets            # polybar
cp -vr $HOME/.conkyrc $out/assets                   # conky
