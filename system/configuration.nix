{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      <home-manager/nixos>
      ./modules/xserver.nix
      ./modules/redshift.nix
      ./modules/sddm.nix
#      ./modules/gnome.nix
    ];

  # installs tmpdir on /tmp
  boot.cleanTmpDir = true;
  boot.tmpOnTmpfs = true;

  boot.loader.grub = {
    enable = true;
    device = "/dev/sda";
    useOSProber = true;
  };

  networking.hostName = "inspiron";
  networking.networkmanager.enable = true;

  time.timeZone = "Europe/London";

  services.printing.enable = true;
  services.printing.drivers = [ pkgs.hplipWithPlugin ];

  # for `locate` command
  services.locate = {
    enable = true;
    locate = pkgs.mlocate;
    interval = "18:00";
    localuser = null;
  };

  console.keyMap = "uk";

  users.users.mips = {
    isNormalUser = true;
    description = "mips";
    extraGroups = [ "networkmanager" "wheel" "audio" "libvirtd" ];
  };

  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.pulseaudio = true;

  nix.settings.experimental-features = "nix-command flakes";

  security.sudo.execWheelOnly = true;

  environment.homeBinInPath = true;

  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.support32Bit = true; 

  system.autoUpgrade.enable = true;
  system.autoUpgrade.allowReboot = false;

  # Virtualisation
  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;

  environment.systemPackages = with pkgs; [
    wget
  ];

  fonts.fonts = with pkgs; [
    (nerdfonts.override { fonts = [
      "FiraCode"
      "JetBrainsMono"
      "Iosevka"
    ]; })
    noto-fonts-cjk
    font-awesome
  ];

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  networking.firewall = {
    enable = true;
    allowedTCPPorts = [
      22
      80
      443
    ];
  };

  programs.vim.defaultEditor = true;

  system.stateVersion = "22.05";
}
