{ config, pkgs, ... }:

{
  i18n.defaultLocale = "en_GB.utf8";

  services.picom.enable = true;

  environment.systemPackages = with pkgs; [
    (callPackage /etc/nixos/modules/pkgs/nordic-wallpapers.nix {})
    polybar
    brightnessctl
  ];

  services.xserver = {
    enable = true;

    layout = "gb";
    xkbVariant = "";

    windowManager.qtile.enable = true;

    desktopManager.xterm.enable = false;

    displayManager = {
      defaultSession = "myqtile";
      session = [{
          manage = "desktop";
          name = "myqtile";
          start = ''
            /home/mips/.fehbg &
            conky &
            /home/mips/.config/polybar/launch.sh &
            exec qtile start
          '';
        }];
      };
  };

}
