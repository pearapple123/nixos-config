{ config, pkgs, ... }:

let qtpkgs = with pkgs.libsForQt5.qt5; [
    qtgraphicaleffects
    qtsvg
    qtquickcontrols
  ];
in {
  services.xserver.displayManager.sddm = {
    enable = true;
    theme = "sugar-candy";
  };

  environment.systemPackages = [
    (pkgs.callPackage ./pkgs/sddm-sugar-candy/default.nix {})
    (pkgs.callPackage ./pkgs/sddm-sugar-dark/default.nix {})
  ] ++ qtpkgs;
 }
