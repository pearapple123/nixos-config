{ config, pkgs, ... }:

{
  location.latitude = 52.370216;
  location.longitude = 4.895168;    
  services.redshift = {
    enable = true;
  
    brightness.day = "1.0";
    brightness.night = "0.8";
  };
}
