{ stdenv
, fetchFromGitHub
, fetchpatch
, lib
}:

stdenv.mkDerivation rec {
  pname = "sddm-sugar-dark";
  version = "1.2";
  dontBuild = true;
  installPhase = ''
    mkdir -p $out/share/sddm/themes
    cp -aRv . $out/share/sddm/themes/sugar-dark
  '';

  patches = [ ./sugar-dark.patch ];

  src = fetchFromGitHub {
    owner = "MarianArlt";
    repo = "sddm-sugar-dark";
    rev = "v${version}";
    sha256 = "0gx0am7vq1ywaw2rm1p015x90b75ccqxnb1sz3wy8yjl27v82yhb";
  };

  meta = with lib; {
    description = "The sweetest dark theme around for SDDM";
    homepage = "https://github.com/MarianArlt/sddm-sugar-dark";
    license = licenses.gpl3Plus;
  };
}
