{ stdenv
, fetchFromGitHub
, lib
}:

stdenv.mkDerivation rec {
  name = "nordic-wallpapers";
  dontBuild = true;

  src = fetchFromGitHub {
    owner = "linuxdotexe";
    repo = "${name}";
    rev = "84ee1b1736772d82a723fe6fa650158a76babc87";
    sha256 = "EqsF4189fWlPvEz9sRwy0Gb4oHYjK7n0SrYYVFb4dfQ=";
  };

  installPhase = ''
    mkdir -pv $out/share/backgrounds/nordic-wallpapers
    cp -aRv ./wallpapers/* $out/share/backgrounds/nordic-wallpapers
  '';

  meta = {
    description = "A collection of wallpapers that go well with the rices inspired by the Nord Colorscheme";
    homepage = "https://github.com/linuxdotexe/nordic-wallpapers";
#    license = lib.licenses.mit;
  };
}
