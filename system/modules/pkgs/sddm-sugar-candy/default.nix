{ stdenv
, fetchFromGitHub
, fetchpatch
, lib
}:

stdenv.mkDerivation rec {
  pname = "sddm-sugar-candy";
  version = "1.6";
  dontBuild = true;
  installPhase = ''
    mkdir -p $out/share/sddm/themes
    cp -aRv . $out/share/sddm/themes/sugar-candy
  '';

  patches = [ ./sugar-candy.patch ];

  src = fetchFromGitHub {
    owner = "Kangie";
    repo = "sddm-sugar-candy";
    rev = "v${version}";
    sha256 = "18wsl2p9zdq2jdmvxl4r56lir530n73z9skgd7dssgq18lipnrx7";
  };

  meta = with lib; {
    description = "The sweetest login theme available for the SDDM display manager";
    homepage = "https://github.com/Kangie/sddm-sugar-candy";
    license = licenses.gpl3Plus;
  };
}
