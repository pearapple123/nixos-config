{ config, pkgs, ... }:

{
  services.xserver.desktopManager.gnome.enable = true;

  environment.gnome.excludePackages = (with pkgs; [
    gnome-photos
    gnome-tour
    gnome-connections
  ]) ++ (with pkgs.gnome; [
    cheese
    gnome-maps
    gnome-weather
    gnome-music
    tali
    iagno
    hitori
    atomix
    gnome-characters
    epiphany
    gnome-contacts
  ]);

  environment.systemPackages = with pkgs; [
    gnome.adwaita-icon-theme
    gnomeExtensions.appindicator
    gnomeExtensions.dash-to-dock
    gnomeExtensions.pop-shell
    gnomeExtensions.caffeine
  ];

  services.udev.packages = with pkgs; [ gnome.gnome-settings-daemon ];
}
