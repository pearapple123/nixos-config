local o = vim.o
local g = vim.g

vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

vim.api.nvim_set_keymap(
  "n",
  "<space>.",
  ":Telescope file_browser",
  { noremap = true }
)

o.number = true

o.termguicolors = true

o.tabstop = 4
o.shiftwidth = 4
o.expandtab = true
o.smarttab = true

o.clipboard = "unnamedplus"
o.wrap = true

g.airline_theme = "transparent"
vim.api.nvim_command('colorscheme tokyonight-night')

-- maps
local function map(m, k, v)
	vim.keymap.set(m, k, v, { silent = true })
end

-- Map <leader> to space
g.mapleader = " "
g.maplocalleader = " "

-- Mimic shell movements
map("i", "<C-E>", "<ESC>A")
map("i", "<C-A>", "<ESC>I")

-- Load recent sessions
map("n", "<leader>sl", "<CMD>SessionLoad<CR>")

-- Keybindings for telescope
map("n", "<leader>fr", "<CMD>Telescope oldfiles<CR>")
map("n", "<leader>ff", "<CMD>Telescope find_files<CR>")
map("n", "<leader>fb", "<CMD>Telescope file_browser<CR>")
map("n", "<leader>fw", "<CMD>Telescope live_grep<CR>")
map("n", "<leader>ht", "<CMD>Telescope colorscheme<CR>")

return require('packer').startup(function(use)
      use 'wbthomason/packer.nvim'

      -- modes
      use 'LnL7/vim-nix'

      -- Dashboard
      use 'nvim-lua/plenary.nvim'
      use 'glepnir/dashboard-nvim'
      
      -- Telescope
      use 'nvim-telescope/telescope.nvim'
      use { "nvim-telescope/telescope-file-browser.nvim" }

      -- Autopairs
      use {
              'windwp/nvim-autopairs',
          config = function() require("nvim-autopairs").setup {} end
          }

      -- misc
      use 'folke/which-key.nvim'
      use 'frazrepo/vim-rainbow'

      -- Vim Airline
      use 'vim-airline/vim-airline'
      use 'vim-airline/vim-airline-themes'

      -- Themes 
      use 'nemixe/moonlight.nvim'
      use 'folke/tokyonight.nvim'

      -- Zen mode
      use 'junegunn/limelight.vim'
      use 'junegunn/goyo.vim'

      if packer_bootstrap then
    	 packer.sync()
      end
end)
