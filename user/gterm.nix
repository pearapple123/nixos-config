{ config, pkgs, ... }:

{
  programs.gnome-terminal = {
    enable = true;
    showMenubar = false;

    profile.b1dcc9dd-5262-4d8d-a863-c897e6d979b9 = {
      default = true;
      visibleName = "my-config";

      showScrollbar = false;
      font = "FiraCode Nerd Font 12";
        
      colors = {
        palette = [
          "#1c2023"
          "#c7ae95"
          "#95c7ae"
          "#aec795"
          "#ae95c7"
          "#c795ae"
          "#95aec7"
          "#c7ccd1"
          "#747c84"
          "#c7ae95"
          "#95c7ae"
          "#aec795"
          "#ae95c7"
          "#c795ae"
          "#95aec7"
          "#f3f4f5"
        ];

        backgroundColor = "#1c2023";
        foregroundColor = "#c7ccd1";
      };

    };

  };

}
