{ config, pkgs, ... }:

let
  aliases = {
    ls = "exa -lh --group-directories-first";
    la = "exa -lha --group-directories-first";
    grep = "grep --color";
    cat = "bat";
    sw = "sudo nixos-rebuild switch";
    confedit = "sudoedit /etc/nixos/configuration.nix";

    hme = "home-manager edit";
    hms = "home-manager switch";

    ga = "git add";
    gs = "git status";
    gd = "git diff";
    gpom = "git push origin main";

    rm = "rm -i";
    cp = "cp -i";
    mv = "mv -i";
  };
in {
  programs.bash.shellAliases = aliases;
  programs.fish.shellAliases = aliases;
}
