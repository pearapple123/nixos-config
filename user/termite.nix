{ config, pkgs, ... }:

{
  programs.termite = {
    enable = true;
    allowBold = true;
    font = "JetBrainsMono Nerd Font 13";

    backgroundColor = "#1c2023";
    foregroundColor = "#c7ccd1";
    colorsExtra = ''
      color0 = #1c2023
      color1 = #c7ae95
      color2 = #95c7ae
      color3 = #aec795
      color4 = #ae95c7
      color5 = #c795ae
      color6 = #95aec7
      color7 = #c7ccd1
      color8 = #747c84
      color9 = #c7ae95
      color10 = #95c7ae
      color11 = #aec795
      color12 = #ae95c7
      color13 = #c795ae
      color14 = #95aec7
      color15 = #f3f4f5
    '';

    searchWrap = true;

    browser = "\${pkgs.qutebrowser}/bin/qutebrowser";
    clickableUrl = true;

    hintsPadding = 5;
  };
}
