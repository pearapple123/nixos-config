{ config, pkgs, ... }:

{
  imports = [
    ./aliases.nix
    ./gtk.nix
    ./gterm.nix
#    ./emacs/emacs.nix
    ./termite.nix
    ./rust.nix
  ];

  home.username = "mips";
  home.homeDirectory = "/home/mips";

  home.stateVersion = "22.05";

  home.packages = with pkgs; [
    rofi
    dmenu
    htop
    pass
    unzip
    killall

    pcmanfm
    thunderbird
    qutebrowser
    firefox
    virt-manager
    feh
    hexchat
    conky
    emacs

    mpv
   
    xclip
    volumeicon
    gv
    libreoffice

    chocolateDoom
  ];

  programs.neovim = {
    enable = true;
    vimAlias = true;
  };

  programs.bash = {
    enable = true;
#    initExtra = "neofetch";
  };

  programs.git = {
    enable = true;
    userName = "GB";
    userEmail = "geraldb@mail.com";
  };

  programs.fish = {
    enable = true;
    shellInit = ''
      set fish_greeting
      '';
  };

  programs.starship = {
    enable = true;
    enableBashIntegration = true;
    enableFishIntegration = true;
    settings = {
      format = "$all$directory$character";
      command_timeout = 5000;
    };
  };

  services.dunst = {
    enable = true;

    iconTheme = {
      package = pkgs.papirus-icon-theme;
      name = "Papirus";
    };

    settings = {
      global = {
        width = 300;
        height = 300;
        offset = "30x50";
        origin = "top-right";
        frame_color = "#eceff1";
        font = "Fira Code Nerd Font 9";
      };

      urgency_normal = {
        background = "#37474f";
        foreground = "#eceff1";
        timeout = 10;
      };
    };

  };

  programs.bat.enable = true;
  programs.exa.enable = true;

  programs.home-manager.enable = true;
}
