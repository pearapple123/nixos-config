{ config, pkgs, ... }:

{
  gtk = {
    enable = true;

    cursorTheme = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Cursors";
    };

    font = {
      package = pkgs.ibm-plex;
      name = "IBM Plex Sans";
    };

    theme = {
      package = pkgs.gnome.gnome-themes-extra;
      name = "Adwaita-dark";
    };

    # Gnome term has no padding by default so need this to add padding manually
    gtk3.extraCss = ''
       VteTerminal,
       TerminalScreen,
       vte-terminal {
         padding: 20px;
         -VteTerminal-inner-border: 20px;
       }
    '';
  };
}
